# Prerequisites
  Django, Python3

# How to Run
  Clone the repository 
  <br /> cd into the Loan_emi_calculator folder.
  <br /> Run the command: python manage.py runserver
  In the first page write respective loan amount,loan interest rate and loan period time.
  After entering this details click on Calculate button it will show the emi on loan.
